require 'test_helper'

class WeatherBackgroundsControllerTest < ActionController::TestCase
  setup do
    @weather_background = weather_backgrounds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:weather_backgrounds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create weather_background" do
    assert_difference('WeatherBackground.count') do
      post :create, weather_background: { condition_id: @weather_background.condition_id, name: @weather_background.name, temperature_id: @weather_background.temperature_id }
    end

    assert_redirected_to weather_background_path(assigns(:weather_background))
  end

  test "should show weather_background" do
    get :show, id: @weather_background
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @weather_background
    assert_response :success
  end

  test "should update weather_background" do
    patch :update, id: @weather_background, weather_background: { condition_id: @weather_background.condition_id, name: @weather_background.name, temperature_id: @weather_background.temperature_id }
    assert_redirected_to weather_background_path(assigns(:weather_background))
  end

  test "should destroy weather_background" do
    assert_difference('WeatherBackground.count', -1) do
      delete :destroy, id: @weather_background
    end

    assert_redirected_to weather_backgrounds_path
  end
end
