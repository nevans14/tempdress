AdminUser.create!([
	{email: 'nicole@yourmodus.com', password: 'Spring2020', password_confirmation: 'Spring2020'},
	{email: 'marc@yourmodus.com', password: 'password', password_confirmation: 'password'},
	{email: 'sam@yourmodus.com', password: 'password', password_confirmation: 'password'}
])
ClothingItem.create!([
  {name: "Striped Shirt", link: "http://macys.com", price: "39.0", image_file_name: "Screen_Shot_2015-12-10_at_11.30.20_PM.png", image_content_type: "image/png", image_file_size: 220059, image_updated_at: "2015-12-11 04:34:02"},
  {name: "Mary Jane Shoes", link: "http://jcpenny.com", price: "29.0", image_file_name: "Screen_Shot_2015-12-10_at_11.34.22_PM.png", image_content_type: "image/png", image_file_size: 142322, image_updated_at: "2015-12-11 04:34:51"},
  {name: "Navy Blue Tights", link: "http://express.com", price: "19.99", image_file_name: "Screen_Shot_2015-12-10_at_11.35.18_PM.png", image_content_type: "image/png", image_file_size: 172669, image_updated_at: "2015-12-11 04:35:44"},
  {name: "fancy belt", link: "http://target.com", price: "19.99", image_file_name: "Screen_Shot_2015-12-10_at_11.36.06_PM.png", image_content_type: "image/png", image_file_size: 48517, image_updated_at: "2015-12-11 04:36:49"},
  {name: "polka dot coat", link: "http://jcrew.com", price: "30.0", image_file_name: "Screen_Shot_2015-12-15_at_6.45.16_PM.png", image_content_type: "image/png", image_file_size: 261062, image_updated_at: "2015-12-15 23:46:43"},
  {name: "Yellow Rain Boots", link: "http://gap.com", price: "17.99", image_file_name: "Screen_Shot_2015-12-15_at_6.45.35_PM.png", image_content_type: "image/png", image_file_size: 153403, image_updated_at: "2015-12-15 23:47:38"}
])
Outfit.create!([
  {condition_id: 5, temperature_id: 1, name: "Fall into Faux Heels", description: "Get spunky with the polka lamp-shade cut jacket from J. Crew.", day: true, visible: true},
  {condition_id: 3, temperature_id: nil, name: "Another Rainy Day", description: "Get spunky with the polka lamp-shade cut jacket from J.Crew", day: true, visible: false}
])
WeatherBackground.create!([
  {name: "Hot and Sunny", condition_id: 5, temperature_id: 1, image_file_name: "Love_Ya_Jess_IMG6599.jpg", image_content_type: "image/jpeg", image_file_size: 12142250, image_updated_at: "2015-11-24 01:48:35"},
  {name: "Rainy", condition_id: 3, temperature_id: nil, image_file_name: "walking_the_rain_umbrella_girl_people_hd-wallpaper-1533993.jpg", image_content_type: "image/jpeg", image_file_size: 178791, image_updated_at: "2015-11-24 02:24:35"}
])
Condition.create!([
  {name: "Thunderstorms", owm_code: 2},
  {name: "Drizzling", owm_code: 3},
  {name: "Rainy", owm_code: 5},
  {name: "Snow", owm_code: 6},
  {name: "Sunny", owm_code: 800},
  {name: "Cloudy", owm_code: 80}
])
OutfitItemDetail.create!([
  {outfit_id: 1, clothing_item_id: 1, column_num: 1, position: 1, tile_size: "large_rectangle"},
  {outfit_id: 1, clothing_item_id: 5, column_num: 1, position: 2, tile_size: "thin_rectangle"},
  {outfit_id: 1, clothing_item_id: 2, column_num: 1, position: 3, tile_size: "square"},
  {outfit_id: 1, clothing_item_id: 3, column_num: 2, position: 1, tile_size: "large_rectangle"},
  {outfit_id: 2, clothing_item_id: 6, column_num: 1, position: 1, tile_size: "large_rectangle"},
  {outfit_id: 2, clothing_item_id: 5, column_num: 1, position: 2, tile_size: "small_rectangle"},
  {outfit_id: 2, clothing_item_id: 7, column_num: 1, position: 3, tile_size: "square"}
])
Temperature.create!([
  {name: "Hot", degree_min: 90, degree_max: nil},
  {name: "Warm", degree_min: 75, degree_max: 89},
  {name: "Cool", degree_min: 65, degree_max: 74},
  {name: "Cold", degree_min: nil, degree_max: 64}
])
