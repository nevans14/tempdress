class CreateWeatherBackgrounds < ActiveRecord::Migration
  def up
    create_table :weather_backgrounds do |t|
      t.string :name
      t.integer :condition_id
      t.integer :temperature_id
      t.timestamps null: false
    end
    add_index :weather_backgrounds, ["condition_id", "temperature_id"]
  end
  def down
    drop_table :weather_backgrounds
  end
end
