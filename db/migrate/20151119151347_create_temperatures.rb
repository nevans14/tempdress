class CreateTemperatures < ActiveRecord::Migration
  def change
    create_table :temperatures do |t|
			t.string "name"
    	t.integer "degree_min"
    	t.integer "degree_max"
      t.timestamps null: false
    end
  end
end
