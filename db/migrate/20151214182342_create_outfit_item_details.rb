class CreateOutfitItemDetails < ActiveRecord::Migration
  
  def change
  	create_table :outfit_item_details do |t| 
	  	t.references :outfit
	  	t.references :clothing_item
	  	t.integer :column_num
	  	t.integer :position
	  	t.string :tile_size
	  	t.timestamps
	  end
	  add_index :outfit_item_details, ["outfit_id", "clothing_item_id"]
  end

end
