class CreateOutfits < ActiveRecord::Migration
  def change
    create_table :outfits do |t|
      t.integer "condition_id"
      t.integer "temperature_id"
      t.string "name"
      t.text "description"
      t.boolean "day", :default => true
      t.boolean "visible", :default => false
      t.timestamps null: false
    end
    add_index("outfits", "condition_id")
    add_index("outfits", "temperature_id")
  end
end
