class CreateClothingItems < ActiveRecord::Migration
  def change
    create_table :clothing_items do |t|
    	t.string "name"
      t.string "link"
      t.decimal "price", :decimal, :precision => 8, :scale => 2
      t.timestamps null: false
    end
  end
end
