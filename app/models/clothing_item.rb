class ClothingItem < ActiveRecord::Base
	has_many :outfit_item_details
  has_many :outfits, :through => :outfit_item_details
	has_attached_file :image, styles: { small: "150x150", med: "375x375", large: "1024x1024" }
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
