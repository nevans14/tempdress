class Condition < ActiveRecord::Base
	has_many :outfits
	has_many :weather_backgrounds
end
