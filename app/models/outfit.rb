class Outfit < ActiveRecord::Base
	
	belongs_to :condition
	belongs_to :temperature

	has_many :outfit_item_details
  has_many :clothing_items, -> { select(
  	'clothing_items.*, 
  	outfit_item_details.position as position,
  	outfit_item_details.column_num as column_num,
  	outfit_item_details.tile_size as size
  	'
  ) }, :through => :outfit_item_details

	accepts_nested_attributes_for :outfit_item_details, :allow_destroy => true

	scope :published, lambda { where(:visible => true)}
	scope :unpublished, lambda { where(:visible => false)}
	scope :day, lambda { where(:day => true)}
	scope :evening, lambda { where(:day => false)}

end
