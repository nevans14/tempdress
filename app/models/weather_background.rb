class WeatherBackground < ActiveRecord::Base
	belongs_to :condition
	belongs_to :temperature
	has_attached_file :image, styles: { small: "150x150", med: "375x375", large: "1024x1024" }
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
