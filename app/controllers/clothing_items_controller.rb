class ClothingItemsController < ApplicationController

  layout "backend"

  before_action :set_clothing_item, only: [:show, :edit, :update, :destroy]

  # GET /clothing_items
  def index
    @clothing_items = ClothingItem.all
  end

  # GET /clothing_items/1
  def show
  end

  # GET /clothing_items/new
  def new
    @clothing_item = ClothingItem.new
  end

  # GET /clothing_items/1/edit
  def edit
  end

  # POST /clothing_items
  def create
    @clothing_item = ClothingItem.new(clothing_item_params)

    if @clothing_item.save
      redirect_to @clothing_item, notice: 'Clothing item was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /clothing_items/1
  def update
    if @clothing_item.update(clothing_item_params)
      redirect_to @clothing_item, notice: 'Clothing item was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /clothing_items/1
  def destroy
    @clothing_item.destroy
    redirect_to clothing_items_url, notice: 'Clothing item was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_clothing_item
      @clothing_item = ClothingItem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def clothing_item_params
      params.require(:clothing_item).permit(:name, :link, :price, :image)
    end
end
