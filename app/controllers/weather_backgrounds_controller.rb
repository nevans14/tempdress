class WeatherBackgroundsController < ApplicationController

  layout false

  before_action :set_weather_background, only: [:show, :edit, :update, :destroy]

  # GET /weather_backgrounds
  def index
    @weather_backgrounds = WeatherBackground.all
  end

  # GET /weather_backgrounds/1
  def show
  end

  # GET /weather_backgrounds/new
  def new
    @weather_background = WeatherBackground.new
  end

  # GET /weather_backgrounds/1/edit
  def edit
  end

  # POST /weather_backgrounds
  def create
    @weather_background = WeatherBackground.new(weather_background_params)

    if @weather_background.save
      redirect_to @weather_background, notice: 'Weather background was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /weather_backgrounds/1
  def update
    if @weather_background.update(weather_background_params)
      redirect_to @weather_background, notice: 'Weather background was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /weather_backgrounds/1
  def destroy
    @weather_background.destroy
    redirect_to weather_backgrounds_url, notice: 'Weather background was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_weather_background
      @weather_background = WeatherBackground.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def weather_background_params
      params.require(:weather_background).permit(:name, :condition_id, :temperature_id, :image)
    end
end
