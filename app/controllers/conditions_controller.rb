class ConditionsController < ApplicationController
  
  layout false

  def index
    @conditions = Condition.all
  end

  def show
    @condition = Condition.find(params[:id])
 end

  def new
    @condition = Condition.new
  end

  def create
    # Instantiate a new object using form parameters
    @condition = Condition.new(condition_params)
    # Save the object
    if @condition.save
      # If save succeeds, redirect to the index action
      redirect_to(:action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
      render('new')
    end
  end

  def edit
  end

  def delete
  end

  private

    def condition_params
      # same as using "params[:condition]", except that it:
      # - raises an error if :condition is not present
      # - allows listed attributes to be mass-assigned
      params.require(:condition).permit(:name, :owm_code)
    end

end

