class TemperaturesController < ApplicationController
  
  layout false

  def index
    @temperatures = Temperature.all
  end

  def show
    @temperature = Temperature.find(params[:id])
  end

  def new
    @temperature = Temperature.new
  end

  def create
    # Instantiate a new object using form parameters
    @temperature = Temperature.new(temperature_params)
    # Save the object
    if @temperature.save
      # If save succeeds, redirect to the index action
      redirect_to(:action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
      render('new')
    end
  end

  def edit
  end

  def delete
  end

  private

    def temperature_params
      # same as using "params[:temperature]", except that it:
      # - raises an error if :temperature is not present
      # - allows listed attributes to be mass-assigned
      params.require(:temperature).permit(:name, :degree_min, :degree_max)
    end

end

