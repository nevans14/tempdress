class MainController < ApplicationController

  #layout false

  def view
  	require 'open_weather'
		require 'json'

  	#@loc = params['loc']
  	@loc = "Miami, FL"

  	@day = (!params[:day]) ? 0 : params[:day]
  	#@day = @day.start_with?( 'day') ? @day.gsub('day', '').to_i : @day

  	@day = case @day
			when "tomorrow"
			  1
			when 0..3
				@day
			else
			  0
		end

		# get current weather by city name
		options = { units: "imperial", APPID: 'c1d8be928127b6a019f1105efb9a2fa9', cnt: 4 }
		@WeatherObj = OpenWeather::Current.city(@loc, options)
		@ForecastObj = OpenWeather::ForecastDaily.city(@loc, options)

		# set variables from returned objects
		@city = @ForecastObj['city']['name']

		@currentweather = @WeatherObj['weather'][0] 
		@currenttemp = @WeatherObj['main']['temp']	

		# daily weather
		@todaytemp = @ForecastObj['list'][0]['temp']
		@todayweather = @ForecastObj['list'][0]['weather'][0]
		@todayclouds = @ForecastObj['list'][0]['clouds']
		@forecasts = @ForecastObj['list']	

		@hash = Hash[@ForecastObj['list'].map.with_index(0) { |v,index|  
			[index,
			"weather" => [
				"min" => v['temp']['min'], 
				"max" => v['temp']['max'], 
				"id" => v['weather'][0]['id'], 
				"clouds" => v['clouds'] ]
			]
		}]
		
		#@hash = @hash.to_json

		# will remove
		@currenttemp = 96
		@currentweather['id'] = 800

		# Gather weather info to select the right outfit

		# Find weather code in db
		@cond_id = Condition.where( "owm_code = ?", @currentweather['id'])
		if @cond_id.empty? 
			# If no match, find a code prefix
			Condition.find_each do |cond|
			  if @currentweather['id'].to_s.start_with?(cond.owm_code.to_s)
			  	@cond_id = cond.id
			  end
			end
		else
			@cond_id = @cond_id.first['id']
		end

		# Find temperature range
		@temp_id = Temperature.where( "? >= degree_min AND ? <= degree_max", @currenttemp, @currenttemp)	
		if @temp_id.empty? 
			# Find hot/cold definitions if no temp range match is found
			if @currenttemp < 75
				@temp_id = Temperature.where( "degree_min IS NULL")
			else
				@temp_id = Temperature.where( "degree_max IS NULL")
			end
		end
		@temp_id = @temp_id.first['id']

		# Find an outfit based on weather conditions
		@outfit = Outfit.find_by(temperature_id: @temp_id, condition_id: @cond_id)
		if @outfit.nil? 
			@outfit = Outfit.find_by(temperature_id: @temp_id, condition_id: nil)
			if @outfit.nil? 
				@outfit = Outfit.find_by(temperature_id: nil, condition_id: @cond_id)
			end
		end

		# Find a background based on weather conditions
		@background = WeatherBackground.find_by(temperature_id: @temp_id, condition_id: @cond_id)
		if @background.nil? 
			@background = WeatherBackground.find_by(temperature_id: @temp_id, condition_id: nil)
			if @background.nil? 
				@background = WeatherBackground.find_by(temperature_id: nil, condition_id: @cond_id)
			end
		end
  end

  def view_day

  	require 'open_weather'

  	@loc = "Miami, FL"

  	# get current weather by city name
		options = { units: "imperial", APPID: 'c1d8be928127b6a019f1105efb9a2fa9', cnt: 4 }
		@WeatherObj = OpenWeather::Current.city(@loc, options)
		@ForecastObj = OpenWeather::ForecastDaily.city(@loc, options)

		# set variables from returned objects
		@city = @ForecastObj['city']['name']

  	@day = (!params[:day]) ? 0 : params[:day].to_i

  	@day = case @day
			when 0..3
				@day
			else
			  0
		end

		# get current weather by city name
		options = { units: "imperial", APPID: 'c1d8be928127b6a019f1105efb9a2fa9', cnt: 4 }
		@WeatherObj = OpenWeather::Current.city(@loc, options)
		@ForecastObj = OpenWeather::ForecastDaily.city(@loc, options)

		# set variables from returned objects
		@city = @ForecastObj['city']['name']

		# daily weather
		@todaytemp = @ForecastObj['list'][@day]['temp']
		@todayweather = @ForecastObj['list'][@day]['weather'][0]
		@todayclouds = @ForecastObj['list'][@day]['clouds']
		@forecasts = @ForecastObj['list']	

		# will remove
		@todaytemp['day'] = 96
		@todayweather['id'] = 500
		@currentweather = @todayweather

		# Gather weather info to select the right outfit

		# Find weather code in db
		@cond_id = Condition.where( "owm_code = ?", @todayweather['id'])
		if @cond_id.empty? 
			# If no match, find a code prefix
			Condition.find_each do |cond|
			  if @todayweather['id'].to_s.start_with?(cond.owm_code.to_s)
			  	@cond_id = cond.id
			  end
			end
		else
			@cond_id = @cond_id.first['id']
		end

		# Find temperature range
		@temp_id = Temperature.where( "? >= degree_min AND ? <= degree_max", @todaytemp['day'], @todaytemp['day'])	
		if @temp_id.empty? 
			# Find hot/cold definitions if no temp range match is found
			if @todaytemp['day'] < 75
				@temp_id = Temperature.where( "degree_min IS NULL")
			else
				@temp_id = Temperature.where( "degree_max IS NULL")
			end
		end
		@temp_id = @temp_id.first['id']

		# Find an outfit based on weather conditions
		@outfit = Outfit.find_by(temperature_id: @temp_id, condition_id: @cond_id)
		if @outfit.nil? 
			@outfit = Outfit.find_by(temperature_id: @temp_id, condition_id: nil)
			if @outfit.nil? 
				@outfit = Outfit.find_by(temperature_id: nil, condition_id: @cond_id)
			end
		end

		# Find a background based on weather conditions
		@background = WeatherBackground.find_by(temperature_id: @temp_id, condition_id: @cond_id)
		if @background.nil? 
			@background = WeatherBackground.find_by(temperature_id: @temp_id, condition_id: nil)
			if @background.nil? 
				@background = WeatherBackground.find_by(temperature_id: nil, condition_id: @cond_id)
			end
		end

		respond_to do |format|
      format.js {}
    end

  end

end
