ActiveAdmin.register Outfit do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

	permit_params :condition_id, :temperature_id, :name, :description, :day, :visible, outfit_item_details_attributes: [:id, :clothing_item_id, :column_num, :position, :tile_size, :_destroy]

	index do
    selectable_column
    column :id
    column :name
    column :description
    column :condition
    column :temperature
    column :day
    column :visible
    column :created_at
    actions
  end

  filter :name
  filter :condition
  filter :temperature
  filter :day
  filter :visible
  filter :created_at

	form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "Outfit", :multipart => true do
      f.input :name
      f.input :description
      f.input :condition, :prompt => "Any"
      f.input :temperature, :prompt => "Any"
      f.input :day
  		f.input :visible
      f.has_many :outfit_item_details do |app_f|
        app_f.input :clothing_item, as: :image_radio, collection: ClothingItem.all
        app_f.input :column_num, :as => :select, :include_blank => false, :collection => {"Left" => 1, "Right" => 2}
        app_f.input :position, :as => :number
        app_f.input :tile_size, :as => :select, :include_blank => false, :collection => {
                                                  "Large Rectange" => "large_rectangle", 
                                                  "Medium Rectange" => "medium_rectangle", 
                                                  "Small Rectange" => "small_rectangle", 
                                                  "Thin Rectange" => "thin_rectangle", 
                                                  "Square" => "square", 
                                                  "Small Square" => "small_square" }
        app_f.input :_destroy, :as => :boolean
      end
    end
    f.actions
  end

end
