ActiveAdmin.register WeatherBackground do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  permit_params :name, :condition_id, :temperature_id, :image

	index do
    selectable_column
    column :id
    column "Image" do |bg|
		  image_tag bg.image.url(:small), class: 'small'
		end
    column :name
    column :condition
    column :temperature
    column :created_at
    actions
  end

  filter :name
  filter :condition
  filter :temperature
  filter :created_at

	form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "WeatherBackground", :multipart => true do
      f.input :name
      f.input :condition, :prompt => "Any"
      f.input :temperature, :prompt => "Any"
      f.input :image
    end
    f.actions
  end

end