class ImageRadioInput < Formtastic::Inputs::RadioInput
  def choice_html(choice)
    
    template.content_tag(
      :label,
      image_preview_content(choice) + builder.radio_button(
        input_name, 
        choice_value(choice), 
        input_html_options.merge(choice_html_options(choice)).merge(:required => false)
      ) << choice_label(choice),
      label_html_options.merge(:for => choice_input_dom_id(choice), :class => nil)
    )

  end

  private
  
  def image_preview_content(choice)
    image_preview?(choice) ? image_preview_html(choice) : ""
  end
  
  def image_preview?(choice)
    @choice = options[:collection].find(choice[1])
    @choice['image_file_name']
  end
  
  def image_preview_html(choice)
    @choice = options[:collection].find(choice[1])
    template.image_tag(@choice.image.url(:small), :class => "image-preview")
  end

end