ActiveAdmin.register ClothingItem do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


	permit_params :name, :link, :price, :image

	index do
    selectable_column
    column :id
    column "Image" do |bg|
		  image_tag bg.image.url(:small), class: 'small'
		end
    column :name
    column :price do |item|
      number_to_currency item.price
    end
    actions
  end

  filter :name
  filter :link
  filter :price

	form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs "ClothingItem", :multipart => true do
      f.input :name
      f.input :link
      f.input :price
      f.input :image, :as => :file, :label => "Image", required: false, :hint => object.image.nil? ? template.content_tag(:span, "No Image Yet") : image_tag(object.image.url(:small)).html_safe

      f.input :_destroy, :as=>:boolean, :required => false, :label => 'Remove image'
    end
    f.actions
  end

end
