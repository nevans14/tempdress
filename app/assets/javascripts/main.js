// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).ready(function(){
	
	$('.day-control a').eq(0).addClass("selected");
	
	$('.day-control a').click(function(){
		$('.day-control a').removeClass("selected");
		$(this).addClass("selected");
	});

});

$( document ).ajaxComplete( function() {

	$( this ).foundation( 'equalizer', 'reflow' );

});